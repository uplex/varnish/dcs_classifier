# $Id$

varnishtest "Test dcs vmod"

server s1 {
	rxreq
	txresp
} -start

varnish v1 -arg "-p workspace_client=9k" -vcl+backend {
	import dcs from "${vmod_topbuild}/src/.libs/libvmod_dcs.so";
	import vtc;

	sub vcl_recv {
	    if (req.xid == "1001") {
		# make sure we run into malloc for first request
		if (vtc.workspace_free(client) <= 1024) {
		    #
		} else if (vtc.workspace_free(client) > 1024) {
		    # has been adjusted
		    vtc.workspace_alloc(client, -1024);
		}
	    }
	    # this code will classify
	    #
	    # for best performance in production, only call classify
	    # once or use inline-C to save the return value of
	    # classify (int)
	    # exotic use cases
	    set req.http.xx-entry-key     = dcs.entry_key(dcs.classify());
	    set req.http.xx-type-id       = dcs.type_id(dcs.classify());

	    # common use cases
	    set req.http.x-nb-classified  = dcs.type_name(dcs.classify());
	    set req.http.X-DeviceClass    = dcs.type_class(dcs.classify());

	    return (synth(200));
	}

	sub vcl_synth {
	    set resp.http.xx-entry-key     = req.http.xx-entry-key;
	    set resp.http.xx-type-id       = req.http.xx-type-id;

	    set resp.http.x-nb-classified  = req.http.x-nb-classified;
	    set resp.http.X-DeviceClass    = req.http.X-DeviceClass;
	    synthetic ({"classified ad here
"});
	    return (deliver);
	}

} -start

# set the workspace too small after the vmod has been initialized
# so we run into the malloc case for the first request

varnish v1 -cliok "param.show workspace_client"
varnish v1 -cliok "param.set workspace_client 9k"
varnish v1 -cliok "param.show workspace_client"

logexpect l1 -v v1 -g raw -d 1 {
	expect * 1001 VCL_call  {^RECV}
	expect 0    = Error     {^notice: workspace_client is set too low}
	expect 0    = Error     {^notice: malloc'ing}
	expect 0    = ReqHeader {^xx-entry-key: unidentified}
	expect 0    = Error     {^notice: malloc'ing}
	expect 0    = ReqHeader {^xx-type-id: 0}
	expect 0    = Error     {^notice: malloc'ing}
	expect 0    = ReqHeader {^x-nb-classified: unidentified}
	expect 0    = Error     {^notice: malloc'ing}
	expect 0    = ReqHeader {^X-DeviceClass: desktop}

	# for the next request, we must not see the notices because
	# workspace should be adjusted now
	expect * 1005 VCL_call  {^RECV}
	expect 0    = ReqHeader {^xx-entry-key: android}
	expect 0    = ReqHeader {^xx-type-id:}
	expect 0    = ReqHeader {^x-nb-classified: Tablet}
	expect 0    = ReqHeader {^X-DeviceClass: tablet}
}

logexpect l1 -start

client c1 {
	# no User-Agent
	txreq -url "/"
	rxresp
	expect resp.status == 200
	expect resp.http.xx-entry-key == "unidentified"
	expect resp.http.xx-type-id == "0"
	expect resp.http.x-nb-classified == "unidentified"
	expect resp.http.X-DeviceClass == "desktop"
} -run

client c1 {
	txreq -hdr "user-agent: willgetignored" -hdr "x-wap-profile: anything"
	rxresp
	expect resp.status == 200
	expect resp.http.xx-entry-key == "generic wap"
	# cant test - can change easily
	# expect resp.http.xx-type-id == "11"
	expect resp.http.x-nb-classified == "Mobile Phone"
	expect resp.http.X-DeviceClass == "smartphone"
} -run

client c1 {
	txreq -hdr "User-Agent: Opera/9.80 (Android; Opera Mini/19.0.1340/34.1309; U; de) Presto/2.8.119 Version/11.10" \
	      -hdr "X-OperaMini-Phone-UA: Mozilla/5.0 (Linux; U; Android 4.3; de-de; ME302C Build/JSS15Q) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Safari/534.30"
	rxresp
	expect resp.status == 200
	expect resp.http.xx-entry-key == "android*me302c"
	# cant test - can change easily
	# expect resp.http.xx-type-id == "11"
	expect resp.http.x-nb-classified == "Tablet"
	expect resp.http.X-DeviceClass == "tablet"
} -run

# append additional headers
client c1 {
	txreq -hdr "User-Agent: motorokr z6/r60_g_80.xx.yyi mozilla/4.0" \
	      -hdr "X-Device-User-Agent: (compatible; msie 6.0 linux;" \
	      -hdr "X-Original-User-Agent: motorokr z6;nnn) profile/midp-2.0" \
	      -hdr "X-Goog-Source: configuration/cldc-1.1 opera 8.50[yy] up.link/6.3.0.0.0 netbiscuits"

	rxresp
	expect resp.status == 200
	expect resp.http.xx-entry-key == "motorokr z6/r60_g_80.xx.yyi mozilla/4.0 (compatible; msie 6.0 linux; motorokr z6;nnn) profile/midp-2.0 configuration/cldc-1.1 opera 8.50[yy] up.link/6.3.0.0.0 netbiscuits"
	# cant test - can change easily
	# expect resp.http.xx-type-id == "11"
	expect resp.http.x-nb-classified == "Mobile Phone"
	expect resp.http.X-DeviceClass == "smartphone"
} -run

logexpect l1 -wait
