/*
 * Copyright (c) 2014 UPLEX - Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS _AS IS_ AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "dcs_config.h"

#ifdef DEBUG_K_MATCH
#include <stdlib.h>
#endif

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <assert.h>

#include "dcs_classifier.h"
#include "dcs_match.h"
	    
#ifdef DEBUG_K_MATCH
#define p_entry(state, fmt, name)					\
	    do {							\
		    if (state->name ## _match_entry) {		\
			    printf(fmt, #name,				\
			    state->name ## _match_entry,		\
			    dcs_type[dcs_entry[state->name ## _match_entry].type], \
			    dcs_entry[state->name ## _match_entry].key); \
		    }							\
	    } while(0)
#define p_entry_if_changed(state, prev_match,fmt)				\
	    do if (state->last_match_entry != prev_match) {		\
			    p_entry(state, fmt, last); \
			    prev_match = state->last_match_entry;	\
		    } while(0)
#else
#define p_entry(...) do { } while(0)
#define p_entry_if_changed(...) do { } while(0)
#endif

int
dcs_match_init(void) {
	if (strcmp(dcs_checksum, DCS_CHECKSUM) != 0) {
		fprintf(stderr, "dcs checksum check failed:\n"
		    "\tgot\t\t%s\n"
		    "\texpected\t%s\n"
		    "Make sure the dcs database you link against is the same you compiled against.\n",
		    dcs_checksum,
		    DCS_CHECKSUM);
		return 1;
	}
	return 0;
}

/*
 * we deliberately return int instead of dcs_entry_id_t here in order to not
 * require dcs_classifier.h for callers
 *
 * mem needs to be an allocation of at least DCS_MATCH_MEM_SZ bytes size
 *  sz is the size of the mem allocation
 */
int /* dcs_entry_id_t */
dcs_match(const char *p, void *mem, size_t sz) {
	struct dcs_matchstate *state = mem;
#ifdef DEBUG_K_MATCH
	dcs_entry_id_t prev_match = 0;
	printf("\n--\n%s",p);
#endif

	assert(p != NULL);
	assert(state != NULL);
	assert(sz >= DCS_MATCH_MEM_SZ);

	dcs_init_matchstate(state);

	while (*p != '\0') {
		dcs_parse_subkey(state, p);
		p_entry_if_changed(state, prev_match, "  %s match %d: %s - %s\n");
		p++;
	}

	dcs_state_eval_candidates(state);
	p_entry_if_changed(state, prev_match, "  new %s from candidates %d: %s - %s\n");
	p_entry(state, " %s match %d: %s - %s\n", min);
	p_entry(state, " %s match %d: %s - %s\n", first);
	p_entry(state, " %s match %d: %s - %s\n", last);

	return (int)state->min_match_entry;
}

#define check_entry_index(entry_index, ret)					\
	do {								\
		if ((entry_index < 0) || (entry_index > (DCS_ENTRY_COUNT - 1))) { \
			errno = EINVAL;					\
			return (ret);					\
		}							\
	} while(0)

int /* enum dcs_type */
dcs_match_id(int entry_index /* dcs_entry_id_t */) {
	check_entry_index(entry_index, -1);
	return (int)dcs_entry[entry_index].id;
}

int /* enum dcs_type */
dcs_match_type_id(int entry_index /* dcs_entry_id_t */) {
	check_entry_index(entry_index, -1);
	return dcs_entry[entry_index].type;
}

const char const *
dcs_match_key(int entry_index /* dcs_entry_id_t */) {
	check_entry_index(entry_index, 0);
	return dcs_entry[entry_index].key;
}

