/*
 * Copyright 2014-2016 UPLEX - Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS _AS IS_ AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * common code for varnish 2 (inline-C) and varnish 3/4 (vmod)
 *
 * this gets _INCLUDED_
 */

#include "dcs_classifier.c"

#include <ctype.h>

#include "dcs_match.c"
#include "dcs_type.c"

#define UA_LIMIT 1024
#define DCS_VARNISH2_NHDRS 4

#if VARNISH_MAJMIN >= 40

typedef const struct vrt_ctx dcs_ctx;

static inline
const char *DCS_GetHdr(dcs_ctx *ctx, const struct gethdr_s *hdr) {
	return VRT_GetHdr(ctx, hdr);
}

/*
 * in varnish 4, the default worker thread stack size has been drastically
 * reduced to 48k, so we take our allocation from the workspace
 */
#define DCS_USE_WS
// malloc
#include <stdlib.h>

#else /* VARNISH_MAJMIN == 30 */

struct gethdr_s {
	enum gethdr_e   where;
	const char      *what;
};

typedef struct sess dcs_ctx;
typedef int VCL_INT;
typedef const char * VCL_STRING;

static inline
const char *DCS_GetHdr(dcs_ctx *ctx, const struct gethdr_s *hdr) {
	return VRT_GetHdr(ctx, hdr->where, hdr->what);
}

#endif

#ifdef DCS_USE_WS
# if VARNISH_MAJMIN < 60
#  undef assert
#  include "cache/cache.h"
# else
#  include "vas.h"
# endif /* VARNISH_MAJMIN < 60 */
# define DCS_USE_WS_MINIMUM PRNDUP(64 * 1024 + DCS_MATCH_MEM_SZ + UA_LIMIT)
#endif


const struct gethdr_s hdr_ua  = {HDR_REQ, "\013User-Agent:"};
const struct gethdr_s hdr_wap = {HDR_REQ, "\016x-wap-profile:"};

const struct gethdr_s hdrs[DCS_VARNISH2_NHDRS] = {
	[0] = { HDR_REQ, "\025X-OperaMini-Phone-UA:" },
	[1] = { HDR_REQ, "\024X-Device-User-Agent:" },
	[2] = { HDR_REQ, "\026X-Original-User-Agent:" },
	[3] = { HDR_REQ, "\016X-Goog-Source:" }
};

const char * const ua_prepend[DCS_VARNISH2_NHDRS] = {
	[0] = " opera/ opera mini/  ",
	[1] = NULL,
	[2] = NULL,
	[3] = NULL
};

#define appnd(w, space, r, l)				\
	do {						\
		l = strlen(r);				\
		strncpy(w, r, space);			\
		if (l > space) {			\
			w += space;			\
			space = 0;			\
			break;				\
		}					\
		space -= l;				\
		w += l;				\
	} while(0)

static int
dcs_varnish_classify(dcs_ctx *ctx) {
	const char *ua = DCS_GetHdr(ctx, &hdr_ua);
	const char *r;
	char *w = NULL;

	int i, ret;

	size_t l;

#ifdef DCS_USE_WS
#define WS_SIZE (DCS_MATCH_MEM_SZ + UA_LIMIT)
	unsigned  space;
	void	 *mem;
	char	 *malloced = NULL;
	char	 *uabuf;

#if VRT_MAJOR_VERSION >= 10
	space = WS_ReserveAll(ctx->ws);
#else
	space = WS_Reserve(ctx->ws, 0);
#endif

	if (space >= WS_SIZE) {
		mem   = ctx->ws->f;
		uabuf = ctx->ws->f + DCS_MATCH_MEM_SZ;
	} else {
		TWEAK_NOTICE(ctx, "malloc'ing ctx->ws: ws %u avail, need %u",
		    space, WS_SIZE);
		space = WS_SIZE;
		malloced = malloc(space);
		AN(malloced);
		mem   = malloced;
		uabuf = malloced + DCS_MATCH_MEM_SZ;
	}

	assert(space >= WS_SIZE);

	space -= DCS_MATCH_MEM_SZ;

	if (space > UA_LIMIT)
		space = UA_LIMIT;
	else
		assert(space == UA_LIMIT);
#else
	size_t space = UA_LIMIT;
	char   mem[DCS_MATCH_MEM_SZ];
	char   uabuf[space];
#endif

	if ((! ua) || (! *ua)) {
		ret = NB_E_SPECIAL_UNIDENTIFIED;
		goto out;
	}

	if (DCS_GetHdr(ctx, &hdr_wap)) {
		ret = NB_E_SPECIAL_GENERIC_WAP;
		goto out;
	}

	/* we need to copy to downcase the string for matching */
	w = uabuf;
	do {
		appnd(w, space, ua, l);
	} while(0);
	ua = uabuf;

	if (space == 0)
		goto nospc;

	for (i = 0; i < DCS_VARNISH2_NHDRS; i++) {
		r = DCS_GetHdr(ctx, &hdrs[i]);
		if (r && *r) {
			if (ua_prepend[i]) {
				appnd(w, space, ua_prepend[i], l);
			} else {
				// space==0 will heave left the loop
				assert(space > 0);
				*w++ = ' ';
				w[1] = '\0';
				if (--space == 0)
					break;
			}
			appnd(w, space, r, l);
		}
	}

  nospc:
	/* ensure the string is always terminated */
	if (w == (uabuf + UA_LIMIT))
		uabuf[UA_LIMIT] = '\0';
	else
		*w = '\0';

	assert(ua == uabuf);
	assert(w > uabuf);
	assert(w <= (uabuf + UA_LIMIT));

	for (w = uabuf; *w; ++w) *w = tolower(*w);

	ret = dcs_match(ua, mem, DCS_MATCH_MEM_SZ);

  out:
#ifdef DCS_USE_WS
	/* used workspace only as scratch - releasing all */
	WS_Release(ctx->ws, 0);
	if (malloced)
		free(malloced);
#endif
	return (ret);
}
