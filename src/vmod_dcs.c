/*
 * Copyright 2014-2016 UPLEX - Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS _AS IS_ AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "dcs_config.h"

#if VARNISH_MAJMIN >= 60
# include "cache/cache_varnishd.h"
# include "vcl.h"
# include "vas.h"
#else
# if VARNISH_MAJMIN >= 41
#  include "vcl.h"
# endif
# include "vrt.h"
#endif

#include "vcc_if.h"

#define TWEAK_NOTICE(ctx, ...) do {					\
		if ((ctx) && (ctx)->vsl) {				\
			VSLb((ctx)->vsl, SLT_Error,			\
			    "notice: " __VA_ARGS__);			\
		} else {						\
			fprintf(stderr, "notice: " __VA_ARGS__);	\
		}							\
	} while (0)

#include "dcs_varnish.c"

static inline void
tweak_ws(dcs_ctx *ctx)
{
	(void)ctx;

#ifdef DCS_USE_WS
#if VARNISH_MAJMIN < 40
	ctx = NULL;
#endif

	if (cache_param->workspace_client >= DCS_USE_WS_MINIMUM)
		return;
	TWEAK_NOTICE(ctx,
	    "workspace_client is set too low for vmod_dcs, "
	    "adjusting from %u to %lu bytes\n",
	    cache_param->workspace_client, DCS_USE_WS_MINIMUM);
	cache_param->workspace_client = DCS_USE_WS_MINIMUM;
#endif
	return;
}

#if VARNISH_MAJMIN >= 41
int
vmod_event(VRT_CTX, struct vmod_priv *priv, enum vcl_event_e e)
{
	(void)priv;

	switch (e) {
	case VCL_EVENT_LOAD:
	case VCL_EVENT_WARM:
		tweak_ws(ctx);
		return (dcs_match_init());
		;;
	case VCL_EVENT_COLD:
		return (0);
	default:
		return (0);
	}
	return (0);
}
#else /* VARNISH_MAJMIN < 41 */
int
vmod_init(struct vmod_priv *priv, const struct VCL_conf *cfg)
{
	(void)priv;
	(void)cfg;

	tweak_ws(NULL);

	return (dcs_match_init());
}
#endif

VCL_INT
vmod_classify(dcs_ctx *ctx) {
	tweak_ws(ctx);
	return (dcs_varnish_classify(ctx));
}

VCL_STRING
vmod_entry_key(dcs_ctx *ctx, VCL_INT e) {
	(void) ctx;
	return dcs_match_key(e);
}

VCL_INT
vmod_type_id(dcs_ctx *ctx, VCL_INT e) {
	(void) ctx;
	return dcs_match_type_id(e);
}

VCL_STRING
vmod_type_name(dcs_ctx *ctx, VCL_INT e) {
	const VCL_INT t = dcs_match_type_id(e);

	(void) ctx;
	return dcs_type_name(t > 0 ? t : 0);
}

VCL_STRING
vmod_type_class(dcs_ctx *ctx, VCL_INT e) {
	const VCL_INT t = dcs_match_type_id(e);

	(void) ctx;
	return dcs_type_class(t > 0 ? t : 0);
}
