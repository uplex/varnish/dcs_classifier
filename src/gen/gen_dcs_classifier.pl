#!/usr/bin/perl

#
# Copyright (c) 2014-2015 UPLEX - Nils Goroll Systemoptimierung
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS _AS IS_ AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use strict;
use warnings;
use Carp;
use Crypt::RC4;
use Digest::MD5 (qw(md5));
use MIME::Base64;
use Fcntl qw(SEEK_SET);

# avoid dependency
# use Carp::Assert;
sub assert ($;$) {
    unless($_[0]) {
	require Carp;
	Carp::confess( _fail_msg($_[1]) );
    }
    return undef;
}

sub should ($$) {
    unless($_[0] == $_[1]) {
	require Carp;
	Carp::confess( _fail_msg($_[0].' == '.$_[1]) );
    }
    return undef;
}

sub shouldeq ($$) {
    unless($_[0] eq $_[1]) {
	require Carp;
	Carp::confess( _fail_msg($_[0].' == '.$_[1]) );
    }
    return undef;
}

sub _fail_msg {
    my($name) = shift;
    my $msg = 'Assertion';
    $msg   .= " ($name)" if defined $name;
    $msg   .= " failed!\n";
    return $msg;
}

sub c_escape ($) {
    my $a = $_[0];
    $a =~ s#(["\\?])#\\$1#g;
    $a;
}

################################################################################
## parser gen and vcl code gen interface

use constant {
    F_MIN		=> 1,
    F_CLASSIFIER_C	=> 1,
    F_CLASSIFIER_H	=> 2,
    F_TYPE_C		=> 3,
    F_MAX		=> 3
};

my @filenames;
$filenames[F_CLASSIFIER_C] = 'dcs_classifier.c';
$filenames[F_CLASSIFIER_H] = 'dcs_classifier.h';
$filenames[F_TYPE_C] = 'dcs_type.c';

{
    my @fhs;

    sub _VCL {
	my ($fid, $where) = (shift, shift);

	assert($filenames[$fid]);

	my $fh = $fhs[$fid];
	unless(defined($fh)) {
	    open ($fh, '>', $filenames[$fid]) ||
		croak('VCL: cant open '.$filenames[$fid].' for writing: '.$!);
	    $fhs[$fid] = $fh;
	}

	print $fh @_;
    }

    sub VCL_close {
	map {
	    if (defined($fhs[$_])) {
		close($fhs[$_]) || warn('error closing '.$filenames[$_].': '.$!);
		undef $fhs[$_];
	    }
	} (F_MIN..F_MAX);
    }
}


use constant {
    VCL_TOP	=> undef
};

my %str2label;
my %label2str;

sub label($) {
    my ($in) = @_;

    if (exists($str2label{$in})) {
	return $str2label{$in};
    }

    my $label = $in;
    $label =~ s/[^a-zA-Z0-9_]/_/g;

    my $i=0;
    while (exists($label2str{$label.$i})) {
	$i++;
    }

    $label2str{$label.$i} = $in;
    $str2label{$in} = $label.$i;

    $label.$i;
}

sub Prefix {
    {
	my $e = scalar(@_);
	return '' if ($e == 0);
	return $_[0] if ($e == 1);
    }

    my $minlen;
    my @d = map {
	my @split = split(//, $_);
	my $l = scalar(@split);
	if (defined($minlen)) {
	    $minlen = $l if ($l < $minlen);
	} else {
	    $minlen = $l;
	}

	\@split;
    } @_;

    my $p = '';
    my $i = 0;
    while($i < $minlen) {
	my $c = $d[0]->[$i];
	return $p unless ($c);
	for (my $j = 1; $j <= $#d; $j++) {
	    return $p unless ($c eq $d[$j]->[$i]);
	}
	$p .= $c;
	$i++;
    }

   $p
}

# this needs two integers declared before inserting the code
#
#  int p, r;
#
# p: current positon in string
# r: result according to input

sub parse_token_code_gen {
    my $varnish = shift;

    my $prevpos	= shift;	# 0, 1 ...
    my $prev	= shift;	# "G", "GE", ...
    my $term	= shift;	# function(macro) name to check if a symbol is terminator
    my $cb	= shift;	# function(macro) to call for each match

    assert ((scalar(@_) % 2) == 0);

    my %symbs	= @_;		# symbol => result, ...

    my $nsymbs = scalar(keys %symbs);

    unless ($nsymbs) {
	_VCL ($varnish, VCL_TOP,  "\tgoto done;\n\n");
	return;
    }

    my $pos = $prevpos + 1;

    # label
    if ($prevpos != -1) {
        _VCL ($varnish, VCL_TOP,  '      '.label('_'.$prevpos.$prev).":\n");
    }

    if ($nsymbs == 1) {
	my ($m, $r) = %symbs;

	my @cond;
	# optimize for just this one string
	my $p;
	for ($p = $pos; $p < length($m); $p++) {
	    push @cond, '(m['.$p."] == '".substr($m, $p, 1)."')";
	}
	push @cond, '('.$term.'(m['.$p."]))";

	_VCL ($varnish, VCL_TOP,  "\t//".$m."\n");
	_VCL ($varnish, VCL_TOP,  "\t".'if ('.join(" && ", @cond).') {'."\n");
	_VCL ($varnish, VCL_TOP,  "\t".'    '.$cb.'('.$p.', '.$r.");\n");
	_VCL ($varnish, VCL_TOP,  "\t"."}\n");
	_VCL ($varnish, VCL_TOP,  "\t".'goto done;'."\n");
    } else {
	my @case;
	my @tocall;

	{
	    ## terminating keys
	    foreach my $m (grep { (length($_) == $pos) } sort keys %symbs) {
		my $r = delete $symbs{$m};

		_VCL ($varnish, VCL_TOP,  "\t//".$m."\n");
		_VCL ($varnish, VCL_TOP,  "\t".'if ('.$term.'(m['.$pos.']))'."\n");
		_VCL ($varnish, VCL_TOP,  "\t	 ".$cb.'('.$pos.', '.$r.");\n");
	    }
	}
	{
	    ## non-terminating keys
	    my @keys = (sort keys %symbs);

	    # get the longest common prefix from here
	    my @prestr = map(substr($_, $pos), @keys);
	    my $prefix = Prefix(@prestr);
	    my $prelen = length($prefix);
	    if ($prelen) {
		# swallow the common prefix
		my @cond;
		my $p;
		for ($p = $pos; $p < $pos + $prelen; $p++) {
		    push @cond, '(m['.$p."] == '".substr($keys[0], $p, 1)."')";
		}
		_VCL ($varnish, VCL_TOP,  "\t".'// common prefix ('.substr($keys[0],0,$pos).')'.$prefix."\n");
		_VCL ($varnish, VCL_TOP,  "\t".'if (! ('.join(" && ", @cond).'))'."\n");
		_VCL ($varnish, VCL_TOP,  "\t".'    goto done;'."\n");
		$pos += $prelen;

		## terminating keys
		foreach my $m (grep { (length($_) == $pos) } @keys) {
		    my $r = delete $symbs{$m};

		    _VCL ($varnish, VCL_TOP,  "\t//".$m."\n");
		    _VCL ($varnish, VCL_TOP,  "\t".'if ('.$term.'(m['.$pos.']))'."\n");
		    _VCL ($varnish, VCL_TOP,  "\t    ".$cb.'('.$pos.', '.$r.");\n");
		}
	    }

	    @keys = (sort keys %symbs);

	    # hash by this char
	    my %h;
	    foreach my $k (@keys) {
		assert( length($k) > $pos, "length($k) > $pos" );
		push @{$h{substr($k, $pos, 1)}}, ($k);
	    }

	    foreach my $k (sort keys %h) {
		my $la = label('_'.$pos.$prev.$k);
		my @me = @{$h{$k}};
		push @case, ("case '".$k."':\tgoto ".$la.";\t// ".
			     join(", ",@me));

		push @tocall, [$prev.$k, \@me];
	    }
	}

	push @case, ("default:\tgoto done;");

	_VCL ($varnish, VCL_TOP,  "\t".'switch (m['.$pos."]) {\n\t");
	_VCL ($varnish, VCL_TOP,  join("\n\t", @case)."\n");
	_VCL ($varnish, VCL_TOP,  "\t}\n");

	if (scalar(@tocall)) {
	    map {
		parse_token_code_gen($varnish, $pos, $_->[0],
				     $term,
				     $cb,
				     (map { $_ => $symbs{$_}; }
				      @{$_->[1]}));
	    } @tocall;
	}
    }
}

################################################################################
## misc

sub boilerplate_autogen_c($$) {
    my ($out, $dbboilerplate) = @_;
    _VCL($out, VCL_TOP, <<'EOF');
/*
 * THIS FILE HAS BEEN AUTOMATICALLY GENERATED. DO NOT EDIT
 *
EOF
# 4emacs */
    if (defined($dbboilerplate) && ($dbboilerplate ne "")) {
	$dbboilerplate =~ s/#/*/go;
	$dbboilerplate =~ s#\*/##go;
	$dbboilerplate =~ s/^\s*\*?/ */mog;

	_VCL($out, VCL_TOP, <<'EOF');
 * the contents of this file have been generated from database files which
 * included the following notices:
 *
EOF
	_VCL($out, VCL_TOP, $dbboilerplate);
	_VCL($out, VCL_TOP, <<'EOF');
 *
 * The following applies to those parts of the generated code which have not
 * been derived from the database file:
 *
EOF
    }
    _VCL($out, VCL_TOP, <<'EOF');
 * Copyright (c) 2014 UPLEX - Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS _AS IS_ AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

EOF
}

################################################################################
## classifier specific

# the peculiar function name is from the apache module sources
sub compute_key($) {
    my ($s, $l) = ($_[0], length($_[0]));

    my $r;
    for (my $i = 0; $i < $l; $i+=4) {
	$r .= substr($s, $i, 1);
    }
    $r;
}

sub load_classifier_db($$) {
    my ($fname, $secret) = @_;

    my $fh;
    open($fh, '<', $fname) || die 'cant open '.$fname.' '.$!;
    binmode($fh);

    # files starting with a # are considered un-encrypted
    my $plainmode;
    die 'error reading '.$fname.' '.$!
	unless (defined(read ($fh, $plainmode, 1)));
    die 'seek error on '.$fname.' '.$!
	unless (seek($fh, 0, SEEK_SET));

    undef $plainmode if ($plainmode ne "#");

    my $plain;
    my $hash_plain;

    unless (defined($plainmode)) {
	my $crypt_data;
	{
	    my $b64;
	    my $sz = (-s $fh);
	    my $res = read ($fh, $b64, $sz);
	    die 'error reading '.$fname.' '.$! if (! defined($res));
	    die 'only read '.$res.' bytes of '.$sz if ($res < $sz);
	    $crypt_data = decode_base64($b64);
	    die 'no data after base64 decode'
		unless (defined($crypt_data) && (length($crypt_data)) > 0);
	}

	$plain = RC4(compute_key($secret), substr($crypt_data, 32));

	die 'no data after decrypt'
	    unless (defined($plain) && (length($plain)) > 0);

	# check hash

	my $h_file = substr($crypt_data, 0, 32);
	$hash_plain = unpack('H*', md5($plain));

	die 'checksum error: file '.$h_file.' decrypt '.$hash_plain
	    if ($h_file ne $hash_plain);
    } else {
	my $sz = (-s $fh);
	my $res = read ($fh, $plain, $sz);
	die 'error reading '.$fname.' '.$! if (! defined($res));
	die 'only read '.$res.' bytes of '.$sz if ($res < $sz);
	$hash_plain = unpack('H*', md5($plain));
	die 'no data from plain file'
	    unless (defined($plain) && (length($plain)) > 0);
    }

    close($fh);

    return wantarray ? (\$plain, $hash_plain) : \$plain;
}

sub enum_name($$) {
    my ($prefix, $s) = @_;

    my $e = $prefix.uc($s);
    $e =~ s:[-\s]:_:g;
    $e =~ s:[^_A-Z0-9]::g;
    return $e
}

my %nbtype2typeenum;
my %typeenum2nbtype;
# convert a NB type string to an enum
sub type_enum($) {
    my $s = $_[0];

    if (exists($nbtype2typeenum{$s})) {
	return $nbtype2typeenum{$s};
    }

    my $e = enum_name('NB_T_', $s);

    if (exists($typeenum2nbtype{$e})) {
	die 'name clash '.$e.' '.$typeenum2nbtype{$e}.' vs '.$s;
    }

    $typeenum2nbtype{$e} = $s;
    $nbtype2typeenum{$s} = \$e;

    \$e;
}

# the format of the classes.conf is the same as that supported by
# Config::IniFiles, but we cannot use this module because it does not
# support empty values

my %class2enum;
my %enum2class;
my %type2classenum;
my $default_class;

sub load_classes($) {
    my ($fname) = @_;
    my %classes;

    die 'classes conf '.$fname.': '.$!
	unless(-f $fname);

    my $fh;
    open($fh, '<', $fname) || die 'cant open '.$fname.' '.$!;

    my $enum;
    while (<$fh>) {
	chomp;
	next if (/^#|^\s*$/);
	if (/^\s*\[([^\]]+)\]/) {
	    my $class = $1;

	    $default_class = $class
		unless(defined($default_class));

	    die $fname.': class '.$class.' used more than once'
		if (exists($class2enum{$class}));

	    my $e = enum_name('T_CLASS_', $class);
	    $enum = \$e;

	    die $fname.': class '.$class.' produces a C symbol name clash with '.$enum2class{$e}.
		' - please rename either'
		if (exists($enum2class{$e}));

	    $enum2class{$e} = $class;
	    $class2enum{$class} = $enum;
	} else {
	    s/\s+$//;

	    if (exists($type2classenum{$_})) {
		die $fname.': type '.$_.' already associated with class '.$enum2class{${$type2classenum{$_}}};
	    }

	    $type2classenum{$_} = $enum;
	}
    }

    close($fh);

    1;
}

sub load_remove($) {
    my ($fname) = @_;
    my @remove;

    if ((! defined($fname)) || ($fname eq "")) {
	return undef;
    }

    my $fh;
    open($fh, '<', $fname) || die 'cant open '.$fname.' '.$!;
    while (<$fh>) {
	chomp;
	my ($id, $key) = split(/\s+/, $_, 2);
	if (defined($remove[$id])) {
	    assert($remove[$id] eq $key);
	    next;
	}
	$remove[$id] = $key;
    }
    close($fh);
    \@remove;
}

sub load_reorder($) {
    my ($fname) = @_;

    # [key, to-id]
    my @reorder;

    if ((! defined($fname)) || ($fname eq "")) {
	return undef;
    }

    my $fh;
    open($fh, '<', $fname) || die 'cant open '.$fname.' '.$!;
    while (<$fh>) {
	chomp;
	my ($a_id, $a_key);
	if (/^\s*(\d+)\s+(.*)/) {
	    ($a_id, $a_key) = ($1, $2);
	} else {
	    die("bad reorder line ".$_);
	}
	$_ = <$fh>;
	chomp;
	my ($b_id, $b_key) = split(/\s+/, $_, 2);
	if ($b_id =~ /^<->(\d+)/) {
	    $b_id = $1;
	} else {
	    die("missing swappy symbol <->");
	}

	if (defined($reorder[$a_id])) {
	    assert($reorder[$a_id]->[0] eq $a_key);
	    if ($b_id > $reorder[$a_id]->[1]) {
		$reorder[$a_id]->[1] = $b_id;
	    }
	} else {
	    $reorder[$a_id] = [$a_key, $b_id];
	};
	if (defined($reorder[$b_id])) {
	    assert($reorder[$b_id]->[0] eq $b_key);
	    if ($a_id < $reorder[$b_id]->[1]) {
		$reorder[$b_id]->[1] = $a_id;
	    }
	} else {
	    $reorder[$b_id] = [$b_key, $a_id];
	};
    }
    close($fh);
    \@reorder;
}

use Getopt::Long;
my %opt;

GetOptions ('key=s'		=> \$opt{key},
	    'classes=s'	=> \$opt{classes},
	    'remove=s'		=> \$opt{remove},
	    'reorder=s'	=> \$opt{reorder})
    || die("Error in command line arguments\n");

# remaining arguments are db files
my @dbrefs;
my @dbnames;
my @dbchecksums;

foreach my $f (@ARGV) {
    my ($dbref, $dbchecksum) = load_classifier_db($f, $opt{key});
    push @dbrefs, ($dbref);
    push @dbnames, ($f);
    push @dbchecksums, ($dbchecksum);
}

load_classes($opt{classes});
my $removeref = load_remove($opt{remove});
my $reorderref = load_reorder($opt{reorder});

my @entries;
use constant {
    ENTRY_INDEX			=> 0,	# index this entry gets in C array
    ENTRY_ID			=> 1,	# id from DB
    ENTRY_ORDER			=> 2,	# order we give it in perl from processing reorder file
    ENTRY_ACTIVE		=> 3,
    ENTRY_COMMON_MATCHMASK	=> 4,
    ENTRY_MATCHMASK		=> 5,
    ENTRY_INITMASK		=> 6,
    ENTRY_TYPE			=> 7,
    ENTRY_KEY			=> 8,
    ENTRY_SUBKEYS		=> 9
};

my %subkeys;
my @subkeys_byid;
use constant {
    SUBKEY_ID			=> 0,
    SUBKEY_STRING		=> 1,
    SUBKEY_COMMON_MATCHMASK	=> 2,
    SUBKEY_ENTRIES		=> 3
};
use constant {
    SUBKEY2ENTRY_ENTRY		=> 0,
    SUBKEY2ENTRY_ENTRYSUBKEY	=> 1
};

use constant {
    # determined by size of matchmask per entry
    MAX_SUBKEY_PER_ENTRY => 32,
    # determined by size of common subkey matchmask
    MAX_COMMON_SUBKEYS => 32,
};

use constant {
    SKSS_KEY			=> 0,
    SKSS_STR			=> 1,	# key without optional !
    SKSS_LEN			=> 2,
    SKSS_NEG			=> 3
};

my %manual_fixup_remove = (
    'android 4.0'	=> 1,
);

my %types;

# id 0 is reserved for the 'no match' case
my $line = 0;
my $subkey_id = 0;

my %special_entries;

# init the special entries
{
    {
	my @e;
	my $s = "unidentified";
	$special_entries{enum_name('NB_E_SPECIAL_',$s)} = $line;
	$e[ENTRY_ACTIVE]	= 1;
	$e[ENTRY_ID]		= 0;
	$e[ENTRY_ORDER]		= $line;
	$e[ENTRY_COMMON_MATCHMASK] = 0xffffffff;
	$e[ENTRY_MATCHMASK]	= 0xffffffff;
	$e[ENTRY_INITMASK]	= 0;
	$e[ENTRY_TYPE]		= type_enum($s);
	$e[ENTRY_KEY]		= \$s;
	$e[ENTRY_SUBKEYS]	= [];
	$entries[$line++]	= \@e;
    }
    {
	my @e;
	my $s = "generic wap";
	$special_entries{enum_name('NB_E_SPECIAL_',$s)} = $line;
	$e[ENTRY_ACTIVE]	= 1;
	$e[ENTRY_ID]		= 0;
	$e[ENTRY_ORDER]		= $line;
	$e[ENTRY_COMMON_MATCHMASK] = 0xffffffff;
	$e[ENTRY_MATCHMASK]	= 0xffffffff;
	$e[ENTRY_INITMASK]	= 0;
	$e[ENTRY_TYPE]		= type_enum("Mobile Phone");
	$e[ENTRY_KEY]		= \$s;
	$e[ENTRY_SUBKEYS]	= [];
	$entries[$line++]	= \@e;
    }
    $special_entries{enum_name('NB_E_SPECIAL_','LIMIT')} = $line;
}

## pre-process the db to generate statistics, filter and
## build %common_subkeys
## build @entries, except that not all fields are filled
my %subkeys_count;
my $dbchecksum = join(", ", @dbchecksums);
my $dbboilerplate = " ** checksums ".$dbchecksum."\n *\n";

for (my $i = 0; $i <= $#dbrefs; $i++) {
    my $dbref = $dbrefs[$i];

    $dbboilerplate .=
	" *\n".
	" ******\n".
	' ** file '.$dbnames[$i].
	' checksum '.$dbchecksums[$i]."\n";

    foreach (split(/[\n\r\f]/, $$dbref)) {
	if (/^\s*#/) {
	    $dbboilerplate .= $_."\n";
	    next;
	}

	# ignore empty lines
	next if (/^\s*$/);

	my ($key, $type) = split(/\t/, $_);

	die "empty key on line ".$line
	    unless (defined($key) && ($key ne ""));
	die "empty type on line ".$line
	    unless (defined($type) && ($type ne ""));

	$key = lc($key);

	# remove any subkey which is wholly contained in another
	# subkey. Depending on which key is positive / negative,
	# we remove the redundant one
	#
	# long | short
	# ------------
	# pos    pos	remove short
	# pos    neg	remove long
	# neg    pos	keep
	# neg    neg	remove long

	my @sks;
	{
	    my @skss = sort {
		$b->[SKSS_LEN] <=> $a->[SKSS_LEN]
	    } map {
		my @ee;
		$ee[SKSS_KEY] = $_;
		if (substr($_, 0, 1) eq "!") {
		    $ee[SKSS_STR] = substr($ee[SKSS_KEY], 1);
		    $ee[SKSS_NEG] = 1;
		} else {
		    $ee[SKSS_STR] = $ee[SKSS_KEY];
		    $ee[SKSS_NEG] = 0;
		}
		$ee[SKSS_LEN] = length($ee[SKSS_STR]);

		\@ee;
	    } split(/\*/, $key);

	  skss:
	    for (my $i = 0; $i <= $#skss; $i++) {
		next skss unless (defined($skss[$i]));

		for (my $j = $i + 1; $j <= $#skss; $j++) {
		    next unless (defined($skss[$j]));

		    if (index($skss[$i]->[SKSS_STR],
			      $skss[$j]->[SKSS_STR]) != -1) {
			# j contained in i
			if ($skss[$j]->[SKSS_NEG]) {
			    # remove long == skip adding i
			    next skss;
			}
			if ($skss[$i]->[SKSS_NEG]) {
			    # long neg   short pos - keep
			} else {
			    # long pos   short pos - remove short
			    undef($skss[$j]);
			}
		    }
		}
		push @sks, ($skss[$i]->[SKSS_KEY]);
		$subkeys_count{$skss[$i]->[SKSS_KEY]}++;
	    }
	}

	my @e;
	$e[ENTRY_ID]		= $line;
	$e[ENTRY_ORDER]	= $line;
	$e[ENTRY_ACTIVE]	= 0;
	$e[ENTRY_TYPE]		= type_enum($type);
	$e[ENTRY_KEY]		= \$key;
	$e[ENTRY_SUBKEYS]	= \@sks;
	$e[ENTRY_INITMASK]	= 0;
	$entries[$line++]	= \@e;
    }
}

# value is bitmask
my %common_subkeys;
{
    # common_subkeys can never be negative
    my @sk_ord = sort { $subkeys_count{$b} <=> $subkeys_count{$a} }
    grep { $_ !~ /^!/ }
    keys %subkeys_count;

    my $i = 0;
    while ($i < MAX_COMMON_SUBKEYS) {
	my $k = $sk_ord[$i];
	last if $subkeys_count{$k} < 3;
	$common_subkeys{$k} = 1 << $i;
	$i++;
    }
}

my $invalid = "*invalid*";
my $manual_fixup = "*manual fixup*";

sub fixup_entry($$) {
    my ($entry, $seen) = @_;

    my $key = ${$entry->[ENTRY_KEY]};

    if ($key =~ m#[\\"]#) {
	warn 'key contains invalid characters - deactivating '.$key;
	$entry->[ENTRY_KEY] = \$invalid;
	return;
    }

    if ($manual_fixup_remove{$key}) {
	warn 'deactivated due to manual fixup: '.$key;
	$entry->[ENTRY_KEY] = \$manual_fixup;
	return;
    }

    if ($removeref->[$entry->[ENTRY_ID]]) {
	if ($removeref->[$entry->[ENTRY_ID]] ne ${$entry->[ENTRY_KEY]}) {
	    die ("bad remove file entry id ".$entry->[ENTRY_ID].
		 " key \"".$removeref->[$entry->[ENTRY_ID]]."\"".
		 " entry \"".${$entry->[ENTRY_KEY]}."\"");
	}
	warn 'deactivated from fixup remove file: '.$key;
	return;
    }


    if ($reorderref->[$entry->[ENTRY_ID]]) {
	shouldeq($reorderref->[$entry->[ENTRY_ID]]->[0],
		 ${$entry->[ENTRY_KEY]});
	$entry->[ENTRY_ORDER] = $reorderref->[$entry->[ENTRY_ID]]->[1];
	warn 'reordered from fixup: id '.$entry->[ENTRY_ID].' order '.$entry->[ENTRY_ORDER];
    }

    if (exists($seen->{$key})) {
	warn 'duplicate key '.$key.' - disabling';
    } else {
	$seen->{$key} = 1;
	$entry->[ENTRY_ACTIVE] = 1;
    }
}

sub process_entry($) {
    my $entry = $entries[$_[0]];
    $entry->[ENTRY_INDEX] = $_[0];

    my $entry_subkey = 0;
    my $common_matchmask = 0;
    my $matchmask = 0;
    my $initmask = 0;

    my @sks = (@{$entry->[ENTRY_SUBKEYS]});

    goto nosubkeys unless(scalar(@sks));

    my %sks_common;
    my %sks_negative;
    my %sks_positive;

    foreach my $subkey (@sks) {
	if (exists($common_subkeys{$subkey})) {
	    $sks_common{$subkey} = 1;
	} elsif ($subkey =~ /^!(.*)/o) {
	    $subkey =~ s/^!//;
	    $sks_negative{$subkey} = 1;
	} else {
	    $sks_positive{$subkey} = 1;
	}
    }

    # we need at least one positive subkey, otherwise the
    # common_matchmask and candidate logic will fail
    if (scalar(keys %sks_positive) == 0) {
	my @common = keys %sks_common;

	die 'neither positive nor common subkeys for key '.
	    ${$entry->[ENTRY_KEY]} unless (scalar(@common) > 0);

	# choose the common subkey with the lowest count
	# (which is the one with the highest value)
	@common = sort { $common_subkeys{$a} <=> $common_subkeys{$a} } @common
	    unless (scalar(@common) == 1);

	$sks_positive{$common[0]} = 1;
	delete $sks_common{$common[0]};
    }

    while (my $subkey = shift @sks) {
	assert($entry_subkey < MAX_SUBKEY_PER_ENTRY);

	my @se;

	if ($sks_common{$subkey}) {
	    $common_matchmask |= $common_subkeys{$subkey};
	} else {
	    my $e;
	    if ($sks_negative{$subkey}) {
		$initmask |= (1<<$entry_subkey);
		$matchmask |= (1<<$entry_subkey);
		$e = 0 - $entry->[ENTRY_INDEX];
	    } else {
		assert($sks_positive{$subkey});
		$matchmask |= (1<<$entry_subkey);
		$e = $entry->[ENTRY_INDEX];
	    }

	    $se[SUBKEY2ENTRY_ENTRY] = $e;
	    $se[SUBKEY2ENTRY_ENTRYSUBKEY] = $entry_subkey;
	    $entry_subkey++;
	}

	if (exists($subkeys{$subkey})) {
	    if (scalar(@se)) {
		push @{$subkeys{$subkey}->[SUBKEY_ENTRIES]}, (\@se);
	    }
	} else {
	    my @n;
	    $n[SUBKEY_ID]		= $subkey_id;
	    $n[SUBKEY_COMMON_MATCHMASK] = exists($common_subkeys{$subkey}) ?
						 $common_subkeys{$subkey} : 0;
	    $n[SUBKEY_STRING]		= \$subkey;
	    $n[SUBKEY_ENTRIES]		= scalar(@se) ? [\@se] : [];

	    $subkeys{$subkey} = \@n;
	    assert(! defined($subkeys_byid[$subkey_id]));
	    $subkeys_byid[$subkey_id] = \@n;
	    $subkey_id++;
	}
    }

  nosubkeys:
    $entry->[ENTRY_COMMON_MATCHMASK]	= $common_matchmask;
    $entry->[ENTRY_MATCHMASK]		= $matchmask;
    $entry->[ENTRY_INITMASK]		= $initmask;
}

{
    my %fixup_seen;
    map { fixup_entry($_, \%fixup_seen); } @entries;
}
@entries = sort { $a->[ENTRY_ORDER] <=> $b->[ENTRY_ORDER] }
	   grep { $_->[ENTRY_ACTIVE] } @entries;

for (my $i = 0; $i <= $#entries; $i++) {
    process_entry($i);
}
my $dcs_entry_count = scalar(@entries);
assert($dcs_entry_count > 0);

my $dcs_type_count = scalar(keys %typeenum2nbtype);

# "

#use Smart::Comments;

### %common_subkeys;
### %nbtype2typeenum;
### %typeenum2nbtype;
### @entries
### %subkeys

################################################################################
## generate the data

boilerplate_autogen_c(F_CLASSIFIER_C, $dbboilerplate);
boilerplate_autogen_c(F_CLASSIFIER_H, undef);
boilerplate_autogen_c(F_TYPE_C, undef);

_VCL (F_CLASSIFIER_H, VCL_TOP, <<EOF);
#ifndef DCS_CLASSIFIER_H
#define DCS_CLASSIFIER_H 1
#include <stdint.h>

#define DCS_CHECKSUM "$dbchecksum"

/* tunable */
#define DCS_MAX_NEGMATCH_CAND	32

typedef unsigned char dcs_seenmask_t;


/* values for the particular dcs db use to generate this code */
#define DCS_TYPE_COUNT $dcs_type_count
#define DCS_ENTRY_COUNT $dcs_entry_count
#define DCS_SUBKEY_COUNT $subkey_id
#define DCS_MATCHSTATE_REGMASK_SZ ((DCS_SUBKEY_COUNT / sizeof(dcs_seenmask_t)) + 1)


/*
 * forward declarations of enums are not allowed, so enum dcs_type cannot be
 * moved into $filenames[F_CLASSIFIER_C]
 *
 * If dcs_type was fixed, we could avoid generating this file and use a static
 * version of it, but as long as each DCS DB may define individual classifier
 * types, can not.
 */
EOF

_VCL (F_CLASSIFIER_H, VCL_TOP,
      "enum dcs_type {\n\t".
      join(",\n\t",
	   'NB_T_UNIDENTIFIED = 0',
	   sort grep { $_ ne 'NB_T_UNIDENTIFIED' } keys %typeenum2nbtype).
      "\n};\n".
      "enum dcs_special_entry {\n\t".
      join(",\n\t",
	   map { $_.' = '.$special_entries{$_} } sort keys %special_entries).
      "\n};\n");

# /* 4emacs c-mode " */
_VCL (F_CLASSIFIER_H, VCL_TOP, <<EOF);

/*
 * when changing these always set the correct _MAX define
 *
 * entry_id needs to be signed because we use the sign to signal negative matches
 */
typedef int16_t dcs_entry_id_t;
#define		DCS_ENTRY_INDEX_MAX	(INT16_MAX - 1)

typedef uint32_t dcs_matchmask_t;

typedef uint16_t dcs_subkey_id_t;
#define		 DCS_SUBKEY_ID_MAX	UINT16_MAX

/* this only needs the number of bits in dcs_entry.matchmask */
typedef uint8_t dcs_entry_subkey_id_t;
#define		DCS_ENTRY_SUBKEY_ID_MAX (sizeof(dcs_matchmask_t) * 4 - 1)

struct dcs_entry {
#define DCS_ENTRY_MAGIC 0x74b979a7
	const uint32_t		magic;

	const dcs_matchmask_t	matchmask;
	const dcs_matchmask_t	common_matchmask;

	const dcs_entry_id_t	id;
	const dcs_entry_id_t	index;		/* redundant, for assertions/debugging */
	const short		active;
	const enum dcs_type	type;
	const char const	*key;		/* for debug */
};

struct dcs_sk2e {
	/*
	 * if entry id is negative, it's a negative match
	 * (and entry id needs to be inverted
	 */
	const dcs_entry_id_t		entry;
	const dcs_entry_subkey_id_t	entry_subkey;
};


struct dcs_subkey {
#define DCS_SUBKEY_MAGIC 0xF15023D3
	const uint32_t			magic;

	const dcs_subkey_id_t		id;
	const dcs_matchmask_t		common_matchmask;
	const char const		*s;	/* debug */

	/* number of entry ids to follow */
	const dcs_entry_id_t		n;
	const struct dcs_sk2e * const	sk2e;
};

struct dcs_matchstate {
#define DCS_MATCHSTATE_MAGIC	0x32bc524d
	uint32_t		magic;
	uint32_t		chk_entry_count;
	uint32_t		chk_subkey_count;

	dcs_entry_id_t		min_match_entry;
	dcs_entry_id_t		first_match_entry;
	dcs_entry_id_t		last_match_entry;
	dcs_matchmask_t		common_matchmask;
	dcs_matchmask_t		matchmask[DCS_ENTRY_COUNT];
	dcs_seenmask_t		seenmask[DCS_MATCHSTATE_REGMASK_SZ];
	dcs_entry_id_t		n_candidates;
	dcs_entry_id_t		candidates[DCS_MAX_NEGMATCH_CAND];
};

const char *dcs_checksum;
const char * const dcs_type[DCS_TYPE_COUNT];
const struct dcs_entry dcs_entry[DCS_ENTRY_COUNT];
const struct dcs_subkey dcs_subkey[DCS_SUBKEY_COUNT];
const struct dcs_matchstate dcs_matchstate_init;

void dcs_init_matchstate (/*\@out\@*/ struct dcs_matchstate *state);
void dcs_parse_subkey(struct dcs_matchstate *state, const char const *m);
void dcs_state_eval_candidates(struct dcs_matchstate *state);

#endif /* DCS_CLASSIFIER_H */
EOF

assert($#subkeys_byid == ($subkey_id - 1));

_VCL (F_CLASSIFIER_C, VCL_TOP, <<EOF);

#include <assert.h>
#include <string.h>

#ifdef DEBUG_SK_MATCH
#include <stdio.h>
#endif

#include "$filenames[F_CLASSIFIER_H]"

const char *dcs_checksum = DCS_CHECKSUM;

EOF

_VCL (F_CLASSIFIER_C, VCL_TOP,
      "const char * const dcs_type[DCS_TYPE_COUNT] = {\n\t".
      join(",\n\t",
	   map { '['.$_.'] = "'.c_escape($typeenum2nbtype{$_}).'"' }
	   sort keys %typeenum2nbtype).
      "\n};\n\n");

_VCL (F_CLASSIFIER_C, VCL_TOP, <<EOF);
const struct dcs_entry dcs_entry[DCS_ENTRY_COUNT] = {
EOF

_VCL (F_CLASSIFIER_C, VCL_TOP,
      join(",\n",
	   map {
	       should($entries[$_]->[ENTRY_INDEX], $_);
	       "\t{ .magic\t= DCS_ENTRY_MAGIC, ".
		   (defined($entries[$_]->[ENTRY_COMMON_MATCHMASK])
		    ? '.common_matchmask = 0x'.unpack("H*", pack("I>", $entries[$_]->[ENTRY_COMMON_MATCHMASK])). ', '
		    : '').
		   (defined($entries[$_]->[ENTRY_MATCHMASK])
		    ? '.matchmask = 0x'.unpack("H*", pack("I>", $entries[$_]->[ENTRY_MATCHMASK])). ', '
		    : '').
		   '.id = '.$entries[$_]->[ENTRY_ID].', '.
		   '.index = '.$_.', /* order '.$entries[$_]->[ENTRY_ORDER].' */ '.
		   '.active = '.$entries[$_]->[ENTRY_ACTIVE].', '.
		   '.type = '.${$entries[$_]->[ENTRY_TYPE]}.', '.
		   '.key = "'.c_escape(${$entries[$_]->[ENTRY_KEY]}).'"}';
	   } (0..$#entries)));
_VCL (F_CLASSIFIER_C, VCL_TOP, <<EOF);
};

const struct dcs_subkey dcs_subkey[DCS_SUBKEY_COUNT] = {
EOF

_VCL (F_CLASSIFIER_C, VCL_TOP,
      join(",\n",
	   map {
	       my $s = $subkeys_byid[$_];
	       should ($_, $subkeys_byid[$_]->[SUBKEY_ID]);
	       "\t{ .magic = DCS_SUBKEY_MAGIC, ".
		   '.id = '.$_.', '.
		   '.common_matchmask = 0x'.unpack("H*", pack("I>", $subkeys_byid[$_]->[SUBKEY_COMMON_MATCHMASK])). ', '.
		   '.s = "'.c_escape(${$subkeys_byid[$_]->[SUBKEY_STRING]}).'", '.
		   (defined($subkeys_byid[$_]->[SUBKEY_ENTRIES]) ?
		   '.n = '.scalar(@{$subkeys_byid[$_]->[SUBKEY_ENTRIES]}).', '.
		   '.sk2e = (const struct dcs_sk2e []){'.join(', ',
			    (map { '{'.join(', ',
					   $_->[SUBKEY2ENTRY_ENTRY],
					   $_->[SUBKEY2ENTRY_ENTRYSUBKEY]).'}' }
			    @{$subkeys_byid[$_]->[SUBKEY_ENTRIES]})).'} }' :
				' }');
	   } (0..$#subkeys_byid)));
_VCL (F_CLASSIFIER_C, VCL_TOP, <<'EOF');
};

const struct dcs_matchstate dcs_matchstate_init = {
	.magic = DCS_MATCHSTATE_MAGIC,
	.chk_entry_count = DCS_ENTRY_COUNT,
	.chk_subkey_count = DCS_SUBKEY_COUNT,

	.min_match_entry = 0,
	.first_match_entry = 0,
	.last_match_entry = 0,
	.common_matchmask = 0,
EOF
_VCL (F_CLASSIFIER_C, VCL_TOP,
      "\t.matchmask = { ".join(",\n\t  ",
		  map { '['.$_->[ENTRY_INDEX].'] = 0x'.unpack("H*", pack("I>", $_->[ENTRY_INITMASK])) } @entries )." },\n");
_VCL (F_CLASSIFIER_C, VCL_TOP, <<'EOF');
	.seenmask = { 0 },
	.n_candidates = 0,
	.candidates = { 0 }
};
EOF

################################################################################
## generate the parse code


_VCL(F_CLASSIFIER_C, undef, <<'EOF');

void dcs_register_subkey_match(struct dcs_matchstate *state, dcs_subkey_id_t subkey_id);

/* we only have subtring matches, so term is always true */
#define PARSE_TERM(c) (1)
#define PARSE_REGISTER(p, r) dcs_register_subkey_match(state, r)
/*
 * parses the string argument and returns the index into the
 * subkey array
 */
void
dcs_parse_subkey(struct dcs_matchstate *state, const char const *m) {
EOF

parse_token_code_gen(F_CLASSIFIER_C, -1, '',
		     'PARSE_TERM',			# function(macro) name to check if a symbol is terminator
		     'PARSE_REGISTER',			# function(macro) to call for each match
		     # make a hash symbol -> SUBKEY_ID
		     map { ($_ => $subkeys{$_}->[SUBKEY_ID]); } (sort keys %subkeys));

_VCL(F_CLASSIFIER_C, undef, <<'EOF');
  done:
	return;
}
#undef PARSE_TERM
#undef PARSE_REGISTER

void
dcs_init_matchstate (/*\@out\@*/ struct dcs_matchstate *state) {
	/* if this fails, fix the init code */
	assert(NB_T_UNIDENTIFIED == 0);

	/* check range of chosen data types */
	assert(DCS_ENTRY_COUNT <= DCS_ENTRY_INDEX_MAX);
	assert(DCS_SUBKEY_COUNT <= DCS_SUBKEY_ID_MAX);

	/*
	 * something must be terribly wrong if the const matchstate_init and
	 * this code don't agree on these counts
	 */
	assert(DCS_ENTRY_COUNT == dcs_matchstate_init.chk_entry_count);
	assert(DCS_SUBKEY_COUNT  == dcs_matchstate_init.chk_subkey_count);
	assert(sizeof(*state) == sizeof(dcs_matchstate_init));

	memcpy(state, &dcs_matchstate_init, sizeof(*state));

	assert(state->magic == DCS_MATCHSTATE_MAGIC);
	assert(state->min_match_entry == 0);
}

static inline int
dcs_register_subkey_match_seen(dcs_seenmask_t *mask, const dcs_subkey_id_t subkey_id) {
	const dcs_subkey_id_t	word = subkey_id / sizeof(dcs_seenmask_t);
	const uint8_t		bit  = subkey_id % sizeof(dcs_seenmask_t);

	assert(word <= DCS_MATCHSTATE_REGMASK_SZ);

	if (mask[word] & (1<<bit))
		return 1;

	mask[word] |= (1<<bit);
	return 0;
}

static inline void
dcs_state_add_candidate(struct dcs_matchstate *state, dcs_entry_id_t cand) {
	dcs_entry_id_t i;

	assert(cand > 0);

#ifndef NDEBUG
	/* we must only add a candidate once */
	for (i = 0; i < state->n_candidates; i++)
		assert(state->candidates[i] != cand);
#endif

	assert(state->n_candidates < DCS_MAX_NEGMATCH_CAND);
	if (! (state->n_candidates < DCS_MAX_NEGMATCH_CAND)) {
		/* XXX THROW ERROR */
		return;
	}

	state->candidates[state->n_candidates] = cand;
	state->n_candidates++;
}

#define dcs_entry_positive(entry_index) (dcs_matchstate_init.matchmask[entry_index] == 0)

/* have got no common_matchmask or all entries hit */
#define dcs_entry_common_match(state, entry_index)					\
	    (dcs_entry[entry_index].common_matchmask == 0) ||				\
	    ((state->common_matchmask & dcs_entry[entry_index].common_matchmask) == 	\
	    dcs_entry[entry_index].common_matchmask)

static inline void
dcs_state_remove_candidate(struct dcs_matchstate *state, dcs_entry_id_t cand) {
	dcs_entry_id_t i, j;

	assert(cand > 0);

	for (i = 0; i  < state->n_candidates; i++) {
		if (state->candidates[i] == cand) {
			for (j = i; j < state->n_candidates - 1; j++)
				state->candidates[j] =
				    state->candidates[j + 1];
			state->n_candidates--;
			assert(state->n_candidates >= 0);
			return;
		}
	}
	/*
	 * it must not happen that we remove something not a candidate
	 * special cases must be handled in dcs_entry gen code
	 * (like only common and negative subkeys as in
	 * iphone*!ipad*!ipod*!freenetmail)
	 */
	assert("removed something not a candidate" == NULL);
}

void
dcs_state_eval_candidates(struct dcs_matchstate *state) {
	dcs_entry_id_t i;

	for (i = 0; i  < state->n_candidates; i++) {
		dcs_entry_id_t entry_index = state->candidates[i];

		if (dcs_entry_common_match(state, entry_index)) {
			if ((state->min_match_entry == 0) ||
			    (entry_index < state->min_match_entry))
				state->min_match_entry = entry_index;
			state->last_match_entry = entry_index;
		}
	}
}



#ifdef DEBUG_SK_MATCH
#define P_SK_MATCH(...) printf(__VA_ARGS__)
#else
#define P_SK_MATCH(...) do { } while(0)
#endif

void
dcs_register_subkey_match(struct dcs_matchstate *state, dcs_subkey_id_t subkey_id) {
	const struct dcs_subkey		*subkey;
	const struct dcs_sk2e		*sk2e;

	dcs_entry_id_t		entry_index, i;

	assert(state->magic == DCS_MATCHSTATE_MAGIC);

	if (dcs_register_subkey_match_seen(state->seenmask, subkey_id)) {
		P_SK_MATCH("register %d - seen before\n", subkey_id);
		return;
	}

	subkey = &dcs_subkey[subkey_id];
	assert(subkey->magic == DCS_SUBKEY_MAGIC);
	assert(subkey->id == subkey_id);

	P_SK_MATCH("register %d\n", subkey_id);

	if (subkey->common_matchmask) {
	    state->common_matchmask |= subkey->common_matchmask;
	}

	sk2e = subkey->sk2e;
	i = 0;
	while (i++ < subkey->n) {
		entry_index = sk2e->entry;
		assert(sk2e->entry_subkey <= DCS_ENTRY_SUBKEY_ID_MAX);
		assert(entry_index != 0);
		/* register the subkey - positive or negative */
		if (entry_index < 0) {
			entry_index = 0 - entry_index;

			if (state->matchmask[entry_index] == dcs_entry[entry_index].matchmask) {
				dcs_state_remove_candidate(state, entry_index);
				P_SK_MATCH("removed candidate index %d id %d this 0x%x mask 0x%x need 0x%x\n",
				    entry_index, dcs_entry[entry_index].id,
				    (1<<sk2e->entry_subkey),
				    state->matchmask[entry_index] & ~((uint32_t)(1 << sk2e->entry_subkey)),
				    dcs_entry[entry_index].matchmask);
			}
			state->matchmask[entry_index] &=  ~((uint32_t)(1 << sk2e->entry_subkey));
		} else {
			state->matchmask[entry_index] |=  ((uint32_t)(1 << sk2e->entry_subkey));
		}

		assert(entry_index > 0);
		assert(entry_index < DCS_ENTRY_COUNT);
		assert(dcs_entry[entry_index].active);
		assert(dcs_entry[entry_index].index == entry_index);


		/*
		 * check for matches
		 *
		 * positive matches:
		 * - if mask ok and common mask ok, we got a match
		 * - if mask ok but not common mask, make it a candidate
		 *
		 * negatives are more complicated:
		 * - if we see the negative subkey before the last match, the matchmasks
		 *   of state and entry will never get eqal -> ok
		 * - otherwise we cannot know that it is a match before we have seen the
		 *   whole string, so make it a candidate for final check
		 *   it is sufficient to check the common mask in the final check
		 */

		if (state->matchmask[entry_index] == dcs_entry[entry_index].matchmask) {
			if (dcs_entry_positive(entry_index)) {
				if (dcs_entry_common_match(state, entry_index)) {
					if ((state->min_match_entry == 0) ||
					    (entry_index < state->min_match_entry))
						state->min_match_entry = entry_index;
					if (state->first_match_entry == 0)
						state->first_match_entry = entry_index;
					state->last_match_entry = entry_index;
					P_SK_MATCH(" match index %d id %d this 0x%x mask 0x%x need 0x%x\n",
					    entry_index, dcs_entry[entry_index].id,
					    (1<<sk2e->entry_subkey),
					    state->matchmask[entry_index],
					    dcs_entry[entry_index].matchmask);
				} else {
					/*
					 * right matchmask but waiting for common_matchmask -
					 * make a candidate
					 */
					dcs_state_add_candidate(state, entry_index);
				}
			} else {
				/* has negative matches */
				dcs_state_add_candidate(state, entry_index);
				P_SK_MATCH("added candidate index %d id %d this 0x%x mask 0x%x need 0x%x\n",
				    entry_index, dcs_entry[entry_index].index,
				    (1<<sk2e->entry_subkey),
				    state->matchmask[entry_index],
				    dcs_entry[entry_index].matchmask);
			}
		}
		sk2e++;
	}
}

EOF

_VCL (F_TYPE_C, VCL_TOP, <<'EOF');
#include "dcs_config.h"

#include <errno.h>

#include "dcs_classifier.h"
#include "dcs_type.h"

#define check_type_id(type_id, ret)						\
	    do {								\
		    if ((type_id < 0) || (type_id > (DCS_TYPE_COUNT - 1))) {	\
			    errno = EINVAL;					\
			    return (ret);					\
		    }								\
	    } while(0)

const char *
dcs_type_name(int type_id /* enum dcs_type */) {
	check_type_id(type_id, 0);
	return dcs_type[type_id];
}

EOF

my %types_unused = %type2classenum;

_VCL (F_TYPE_C, VCL_TOP,
      "enum dcs_type_class {\n\t".
      join(",\n\t",
	   '_T_CLASS_MISSING = 0',
	   sort keys %enum2class,
	   '_T_CLASS_LIMIT').
      "\n};\n\n".
      "const char * const dcs_type_class_str[_T_CLASS_LIMIT] = {\n\t".
      join(",\n\t",
	   '[_T_CLASS_MISSING] = "'.$default_class.'"',
	   map { '['.$_.'] = "'.$enum2class{$_}.'"' } sort keys %enum2class).
      "\n};\n\n".
      "enum dcs_type dcs_type2class[DCS_TYPE_COUNT] = {\n\t".
      join(",\n\t",
	   map {
	       my $class_enum = $type2classenum{$_};

	       if (defined($class_enum)) {
		   delete $types_unused{$_};
	       } else {
		   warn 'No class assignment for type '.$_.' - mapping to default '.$default_class;
		   $class_enum = \"_T_CLASS_MISSING";
	       }
	       # 4emacs "}

	       '['.${type_enum($_)}.'] = '.$$class_enum
	   } sort keys %nbtype2typeenum).
      "\n};\n\n");

_VCL (F_TYPE_C, VCL_TOP, <<EOF);
const char *
dcs_type_class(int type_id /* enum dcs_type */) {
	check_type_id(type_id, 0);
	return (dcs_type_class_str[dcs_type2class[type_id]]);
}
EOF

if (scalar(%types_unused)) {
    warn 'Unused type(s) in classes.conf: '.join(", ", sort keys %types_unused);
}


VCL_close();
