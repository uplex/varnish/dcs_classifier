/*
 * Copyright (c) 2014 UPLEX - Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS _AS IS_ AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "dcs_config.h"

#include <stdio.h>

#include "dcs_classifier.h"

/*
 * generate the dcs_match_mem.h file telling callers of dcs_match the size of
 * the required private data structure (struct dcs_matchstate)
 */

static const char *pre =
    "/*\n"
    " * THIS FILE HAS BEEN AUTOMATICALLY GENERATED. DO NOT EDIT\n"
    " */\n\n"
    "#ifndef DCS_MATCH_MEM_H\n"
    "#define DCS_MATCH_MEM_H 1\n\n";

static const char *post =
    "\n"
    "#endif\n";

int
main(void) {
	printf("%s", pre);
	printf("#define DCS_MATCH_MEM_SZ %zu "
	    "/* sizeof(struct dcs_matchstate) */\n",
	    sizeof(struct dcs_matchstate));
	printf("%s", post);
}
