/*
 * Copyright (c) 2014-2015 UPLEX - Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS _AS IS_ AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "dcs_config.h"

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <assert.h>

#include "gen/dcs_classifier.h" /* need DCS_* */
#include "dcs_match.h"

int test (const char *, const char *);
static void fixup_out (FILE *, FILE *, const int, const int);

int
main (int argc, char *argv[]) {
	dcs_match_init();

	if (argc > 3) {
		fprintf(stderr, "Too many arguments");
		return (-1);
	}
	if (argc == 3)
		return test(argv[1], argv[2]);
	if (argc == 2)
		return test(argv[1], NULL);
	if (argc == 1)
		return test(NULL, NULL);
}

static void
test_p_diff (const char *label, const int i, const char *testkey, const int r) {
	if (testkey && *testkey && testkey != dcs_entry[i].key)
		printf("%s\tteststring\t\t\t%s\n", label, testkey);
	else
		printf("%s", label);

	if (dcs_entry[i].type != dcs_entry[r].type) {
		printf("\ti: %8d (id %8d)\t%s\t%s\n"
		    "type\tr: %8d (id %8d)\t%s\t%s\n",
		    i, dcs_entry[i].id,
		       dcs_entry[i].key, dcs_type[dcs_entry[i].type],
		    r, dcs_entry[r].id,
		       dcs_entry[r].key, dcs_type[dcs_entry[r].type]);
	} else {
		printf("\ti: %8d (id %8d)\t%s\n"
		    "\tr: %8d (id %8d)\t%s\n",
		    i, dcs_entry[i].id, dcs_entry[i].key,
		    r, dcs_entry[r].id, dcs_entry[r].key);
	}
}

#ifdef NDEBUG
#define AP(x)	x
#else
#define AP(x)	assert((x) >= 0)
#endif

static void
fixup_out (FILE *f_remove, FILE *f_reorder, const int i, const int r) {
	if (dcs_entry[i].type != dcs_entry[r].type) {
		if (f_reorder)
			AP(fprintf(f_reorder, "   %i %s\n<->%i %s\n",
			    dcs_entry[i].id, dcs_entry[i].key,
			    dcs_entry[r].id, dcs_entry[r].key));
	} else {
		if (f_remove)
			AP(fprintf(f_remove, "%i %s\n",
			    dcs_entry[i].id, dcs_entry[i].key));
	}
}

#define KEYLIM 256

int
test (const char *fixup_remove_name, const char *fixup_reorder_name) {
	char mem[DCS_MATCH_MEM_SZ];
	int i, r, errcount = 0;

	FILE *f_remove = NULL, *f_reorder = NULL;

	if (fixup_remove_name && *fixup_remove_name) {
		if ((f_remove = fopen(fixup_remove_name, "a")) == NULL) {
			perror("remove_file: ");
			return -1;
		}
	}

	if (fixup_reorder_name && *fixup_reorder_name) {
		if ((f_reorder = fopen(fixup_reorder_name, "a")) == NULL) {
			perror("reorder_file: ");
			if (f_remove)
				fclose(f_remove);
			return -1;
		}
	}

	for (i = NB_E_SPECIAL_LIMIT; i < DCS_ENTRY_COUNT; i++) {
		if (dcs_entry[i].active == 0)
			continue;

		if (dcs_matchstate_init.matchmask[i] == 0) {
			/* only positive matches */

			char nkey[KEYLIM];
			int  pr = -1;
			const char *prk = NULL;

			nkey[0] = '\0';

			r = dcs_match(dcs_entry[i].key, mem, DCS_MATCH_MEM_SZ);
			while (i != r) {
				const char *ntok, *ntoke;

				/*
				 * check if the entry we have hit contains
				 * negative tokens and, if yes, retry with all
				 * of them (in succession)
				 */
				ntok = NULL;

				/*
				 * end the recursion when we get an earlier
				 * match than previous
				 */
				if (pr != -1 && r < pr) {
					test_p_diff("miss r", i, NULL, r);
					fixup_out(f_remove, f_reorder, i, r);
					errcount++;
					break;
				}

				if (pr != r) {
					pr  = r;
					prk = dcs_entry[r].key;

					if (prk[0] == '!')
						ntok = prk++;
				}

				if (ntok == NULL &&
				    (ntok = strstr(prk++, "*!")) != NULL)
					ntok += 1;

				if (ntok == NULL) {
					test_p_diff("miss", i, NULL, r);
					fixup_out(f_remove, f_reorder, i, r);
					errcount++;
					break;
				}

				assert(strlen(dcs_entry[r].key) < KEYLIM);

				/*
				 * append the negative token from entry r to
				 * entry i
				 */
				if (nkey[0] == '\0')
					strcpy(nkey, dcs_entry[i].key);

				if ((ntoke = strchr(ntok, '*')) != NULL) {
					assert((strlen(nkey) + (ntoke - ntok))
					    < KEYLIM);
					strncat(nkey, ntok, (ntoke - ntok));
				} else {
					assert((strlen(nkey) + strlen(ntok))
					    < KEYLIM);
					strcat(nkey, ntok);
				}
				r = dcs_match(nkey, mem, DCS_MATCH_MEM_SZ);
			}
		} else {
			const char *poskey = dcs_entry[i].key;
			const char *negkey = dcs_entry[i].key;
			char p1[KEYLIM], p2[KEYLIM];
			char n1[KEYLIM], n2[KEYLIM];
			char *save;

			if (poskey[0] == '!' ||
			    strstr(poskey, "*!")) {
				const char *pp, *np;

				assert(strlen(poskey) < KEYLIM);
				assert(strlen(negkey) < KEYLIM);

				strcpy(p1, poskey);
				strcpy(n1, negkey);

				/* extract only positive tokens */
				*p2 = '\0';
				poskey = p2;

				if ((pp = strtok_r(p1, "*", &save))) {
					while (pp && *pp == '!')
						pp = strtok_r(NULL, "*", &save);

					if (pp)
						strcat(p2, pp);

					while ((pp =
						strtok_r(NULL, "*", &save))) {

						if (*pp == '!')
							continue;

						strcat(p2, "*");
						strcat(p2, pp);
					}
				}

				/* extract negative tokens also */
				*n2 = '\0';
				negkey = n2;

				if ((np = strtok_r(n1, "*", &save))) {
					if (*np == '!')
						np++;

					strcat(n2, np);

					while ((np =
						strtok_r(NULL, "*", &save))) {
						if (*np == '!')
							np++;

						strcat(n2, "*");
						strcat(n2, np);
					}
				}
			}

			r = dcs_match(poskey, mem, DCS_MATCH_MEM_SZ);
			if (i != r) {
				test_p_diff("miss np", i, poskey, r);
				fixup_out(f_remove, f_reorder, i, r);
				errcount++;
			}

			r = dcs_match(negkey, mem, DCS_MATCH_MEM_SZ);
			if (r == i) {
				test_p_diff("miss nn", i, negkey, r);
				fixup_out(f_remove, f_reorder, i, r);
				errcount++;
			}
		}
	}
	return errcount;
}
